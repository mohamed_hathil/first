import React,{Component} from 'react';
import { NavDropdown,Nav,NavLink } from 'react-bootstrap';
export class Sidebar extends Component{
    render(){
        return(
            <div className="sidebar">
          <h3>MENU</h3>
          <Nav className="nav">
        <NavLink href="/" activeClassName='is-active'>Home</NavLink>
        <NavLink href="/profile" activeClassName='is-active'>MY PROFILE</NavLink>
        <NavLink href="/quote" activeClassName="active-link">GET QUOTE</NavLink>
        <NavLink href="/log" activeClassName="active-link">LOGOUT</NavLink>
           <NavDropdown.Divider />
           </Nav>
      

            </div>
        );
    };
}