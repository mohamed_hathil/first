import logo from './logo.svg';
import './App.css';
import {Route,BrowserRouter as Router,Switch } from 'react-router-dom';

import { Home } from './components/Home';
import { Profile } from './components/profile';
import { QUOTE } from './components/quote';
import { Log } from './components/log';

function App() {
  return (
    <div className="App">
      
      <Router>
 
           
<Switch>


<Route  exact path="/" component={Home}/>
<Route  exact path="/profile" component={Profile}/>
<Route  exact path="/quote" component={QUOTE}/>
<Route  exact path="/log" component={Log}/>
 
 

  </Switch>
</Router>
    
    </div>
  );
}

export default App;
