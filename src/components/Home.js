import React,{Component} from "react";
import { Sidebar } from "./sidebar";
import { Col, Row,Dropdown, } from "react-bootstrap";
import{FaArrowRight,FaHome,FaBuilding,FaTachometerAlt} from 'react-icons/fa'
import { BsCalendarDate,BsFillExclamationTriangleFill,BsCheckLg} from "react-icons/bs";

export class Home extends Component{
  
         constructor(){
             super()
             this.state={
                 showMe:false,
                 showus:false,
                 detail:false
             }
         }
         logic(){
this.setState({
    showMe:!this.state.showMe,
 
})
         }


         logic1(){
            this.setState({  
                 showus:!this.state.showus,
                 detail:!this.state.detail
                
            })
                     }



        
    render(){
        return(

             <div className="home">
     <Row>
             <Col lg={2 }md={4} sm={6} xs={5}>
                 <Sidebar/>
 </Col>
 
 
             <Col lg={10} md={8} sm={6} xs={7}>    
               <h1>Home</h1>
                 <Row>
                     <Col lg={3}md={3} xs={12}>
 
                     <div className="adrs"> <h6>from</h6>
                            <p>ejpura,bengalur,karnataka</p>
                             </div>
                             </Col>

                             <Col lg={3} md={3} xs={12} className="text-center">
                             <FaArrowRight className="arrow"/>
                             </Col>

                             <Col lg={3} md={3} xs={12} >

                            <div className="adrs1">
                                   <h6>To</h6>
                            <p>Hsr Layout,Bengalur, karnataka</p>
                             
                            </div>
                            </Col>
                           
                          


 <Col lg={3} md={3} xs={12}>  <div className="adrs2">
    <h4>Request</h4>
                            <p>E21418</p>
                     </div>   
                    
                     
 </Col>  
  </Row> 

<Row>
    <Col lg={4} sm={4} xs={12}>
    <div className="all">
    <p><FaHome className="icon"/>1BHK</p> 
<p><FaBuilding className="icon"/>32</p>
 <p><FaTachometerAlt className="icon"/>8.3km</p>
 </div>
</Col>
<Col lg={4} sm={4} xs={12}>
<div className="all">
<p><BsCalendarDate className="icon"/>sep 26,2020 at 6:18pm</p> 
<p><BsCheckLg className="icon"/>is flexible</p>
</div>
</Col>
<Col lg={4} sm={4} xs={12} className="mt-3 mx-auto">
<div className="all">
<button onClick={()=>this.logic()}>view more details</button>
<button>
    quotes awaiting
</button>
</div>
    

</Col>

</Row>

<Row className="footer mx-auto">
    <Col xs={12}>  <p><BsFillExclamationTriangleFill className="icon"/><span className="bold">
     disclaimer:
 </span>please update your move date before two days of shifting</p>
    </Col>
    </Row>
    <Row>
    {
                                   this.state.showMe?
                                   <div className="toggle">
                                         <h6>INVENTORY DETAILS</h6>
             <h6 className="text" onClick={()=>this.logic1()}>LIVING ROOM <span>15</span></h6>
             
                                  { 
                                      this.state.detail?
                                   <div className="details">
                                       <Row>
                                           <Col xs={12} lg={4} sm={4}>
                                                  <div className="div1">
                                                  <h4>furnitures</h4>
                                                  <ul>
                                                      <li>1 seater sofa</li>
                                                      <li>3 seater sofa</li>
                                                      <li>Tv cabinet</li>
                                                      <li>Study table</li>
                                                      <li>shoe rack</li>
                                                  </ul>
                                                  </div>
                                           </Col>
                                           <Col xs={12} lg={4} sm={4}>
                                               <div className="div2">
                                                  <h4>ELectricals</h4>
                                                  <ul>
                                                      <li>Lcd Tv</li>
                                                      <li>HOME theater</li>
                                                      <li>CEILING fan</li>
                                                  </ul>
                                           </div>
                                           </Col>
                                           <Col xs={12}lg={4} sm={4} >
                                                <div className="div3">
                                           <h4>FRAGILE</h4>
                                           <ul>
                                               <li>
                                                   Bulb
                                               </li>
                                               <li>painting</li>
                                               <li>clock</li>
                                           </ul>
                                           </div>
                                           </Col>
                                       </Row>
                                        
                                       
                                          
                                      
                                </div>
                                       :null}
                                       
             <h6 className="text">BED ROOM <span>6</span></h6>
             <h6 className="text">kitchen <span>7</span></h6>
             <h6 className="text">Bathroom <span>6</span></h6>
          
                                     </div>
                                     :null
                               }
                     
    </Row>
 <Dropdown.Divider />



 <Row>
                     <Col lg={3}md={3} xs={12}>
 
                     <div className="adrs"> <h6>from</h6>
                            <p>ejpura signal 100 first road chandra rickly Layoutbengalur karnataka</p>
                             </div>
                             </Col>

                             <Col lg={3} md={3} xs={12} className="text-center">
                             <FaArrowRight className="arrow"/>
                             </Col>

                             <Col lg={3} md={3} xs={12} >

                            <div className="adrs1">
                                   <h6>To</h6>
                            <p>Ulsoor lake ulsoor lake pathway,rukmani colony,sivanchetti garden,bengalur karnataka</p>
                             
                            </div>
                            </Col>
                           
                          


 <Col lg={3} md={3} xs={12}>  <div className="adrs2">
    <h4>Request</h4>
                            <p>E43057</p>
                     </div>   
                    
                     
 </Col>  
  </Row> 

<Row>
    <Col lg={4} sm={4} xs={12}>
    <div className="all">
    <p><FaHome className="icon"/>3+BHK</p> 
<p><FaBuilding className="icon"/>82</p>
 <p><FaTachometerAlt className="icon"/>8.3km</p>
 </div>
</Col>
<Col lg={4} sm={4} xs={12}>
<div className="all">
<p><BsCalendarDate className="icon"/>sep 16,2020 at 7:28pm</p> 
<p><BsCheckLg className="icon"/>is flexible</p>
</div>
</Col>
<Col lg={4} sm={4} xs={12} className="mt-3 mx-auto">
<div className="all">
<button>view more details</button>
<button>
    quotes awaiting
</button>
</div>
    

</Col>

</Row>


  </Col>
 </Row>
 
                          
  
              

     </div>
     )
     }
    }
     
      
                   
                        